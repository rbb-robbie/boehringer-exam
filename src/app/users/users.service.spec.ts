import { TestBed } from "@angular/core/testing";

import { UsersService } from "./users.service";
import { HttpClientModule } from "@angular/common/http";

describe("UsersService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    })
  );

  it("should get users list", done => {
    const service: UsersService = TestBed.get(UsersService);
    service.listUsers().subscribe(users => {
      expect(Object.keys(users[0])).toContain("username");
      done();
    });
  });
  it("should be created", () => {
    const service: UsersService = TestBed.get(UsersService);
    expect(service).toBeTruthy();
  });
});
