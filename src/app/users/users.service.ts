import { Injectable } from "@angular/core";
import { User } from "./user";
import { Observable, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class UsersService {
  constructor(protected http: HttpClient) {}

  public listUsers(): Observable<User[]> {
    const handler = new Subject<User[]>();
    this.http
      .get("https://jsonplaceholder.typicode.com/users")
      .subscribe(
        (users: User[]) => handler.next(users),
        (error: Error) => handler.error(error),
        () => handler.complete()
      );
    return handler.asObservable();
  }
  public getUser(id: number): Observable<User> {
    const handler = new Subject<User>();
    this.http
      .get(`https://jsonplaceholder.typicode.com/users/${id}`)
      .subscribe(
        (user: User) => handler.next(user),
        (error: Error) => handler.error(error),
        () => handler.complete()
      );
    return handler.asObservable();
  }
}
