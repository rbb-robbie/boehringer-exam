import { Component, OnInit } from "@angular/core";
import { Post, PostComment } from "../post";
import { User } from "src/app/users/user";
import { PostsService } from "../posts.service";
import { UsersService } from "src/app/users/users.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { of, merge, from } from "rxjs";

@Component({
  selector: "boin-post-detail",
  templateUrl: "./post-detail.component.html",
  styleUrls: ["./post-detail.component.scss"]
})
export class PostDetailComponent implements OnInit {
  public post: Post;
  public user: User;
  public comments: PostComment[];

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected postService: PostsService,
    protected usersService: UsersService,
    protected snacks: MatSnackBar
  ) {}

  ngOnInit() {
    this.postService.postDetail(this.route.snapshot.params.id).subscribe(
      post => {
        this.usersService.getUser(post.userId).subscribe(user => {
          this.post = post;
          this.user = user;
        });
      },
      error => {
        if (error.status === 404) {
          this.snacks.open("The requested post doesn't exists.", null, {
            duration: 2500
          });
          this.router.navigate(["/post"]);
        }
      }
    );
  }
  public loadComments() {
    this.postService.postComments(this.post.id).subscribe(
      comments => {
        if (comments.length) {
          this.comments = comments;
        } else {
          this.snacks.open("No comments found.", null, {
            duration: 1500
          });
        }
      },
      error =>
        this.snacks.open(
          "Something went wrong. Please, try again later",
          null,
          {
            duration: 1500
          }
        )
    );
  }
  public delete(id: number) {
    this.postService.removePost(id).subscribe(
      () =>
        this.snacks.open("Post successfully deleted.", null, {
          duration: 1500
        }),
      error =>
        this.snacks.open("Wasn't able to delete this post.", null, {
          duration: 1500
        })
    );
  }
}
