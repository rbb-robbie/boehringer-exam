import { TestBed, inject } from "@angular/core/testing";

import { PostsService } from "./posts.service";
import { HttpClientModule, HttpClient } from "@angular/common/http";

describe("PostsService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    })
  );
  it("should create post", done => {
    const service: PostsService = TestBed.get(PostsService);
    service
      .newPost({
        title: "foo",
        body: "bar",
        userId: 1
      })
      .subscribe(post => {
        expect(post.id).toEqual(101);
        expect(post.userId).toEqual(1);
        done();
      });
  });
  it("should fetch post WHERE ID = 1 comments ", done => {
    const service: PostsService = TestBed.get(PostsService);
    service.postComments(1).subscribe(comments => {
      expect(Object.keys(comments[0])).toContain("postId");
      done();
    });
  });
  it("should fail when no post found", done => {
    const service: PostsService = TestBed.get(PostsService);
    service.postDetail(99999).subscribe(
      post => {},
      error => {
        expect(error.status).toEqual(404);
        done();
      }
    );
  });
  it("should fetch post WHERE ID = 1", done => {
    const service: PostsService = TestBed.get(PostsService);
    service.postDetail(1).subscribe(post => {
      expect(post.id).toEqual(1);
      done();
    });
  });
  it("should filter posts WHERE userID = 2", done => {
    const service: PostsService = TestBed.get(PostsService);
    service.listPosts({ userId: 2 }).subscribe(posts => {
      expect(posts[0].userId).toEqual(2);
      done();
    });
  });
  it("should fetch posts", done => {
    const service: PostsService = TestBed.get(PostsService);
    service.listPosts().subscribe(posts => {
      expect(Object.keys(posts[0])).toContain("userId");
      done();
    });
  });
  it("should be created", () => {
    const service: PostsService = TestBed.get(PostsService);
    expect(service).toBeTruthy();
  });
});
