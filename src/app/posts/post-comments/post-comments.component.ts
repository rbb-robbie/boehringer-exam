import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "boin-post-comment",
  templateUrl: "./post-comments.component.html",
  styleUrls: ["./post-comments.component.scss"]
})
export class PostCommentsComponent implements OnInit {
  @Input() comment: Comment;

  constructor() {}

  ngOnInit() {}
}
