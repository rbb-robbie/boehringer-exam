import { Component, OnInit } from "@angular/core";
import { PostsService } from "../posts.service";
import { Post } from "../post";
import { Observable } from "rxjs";

import { MatSnackBar } from "@angular/material/snack-bar";
import { UsersService } from "src/app/users/users.service";
import { User } from "src/app/users/user";
import { MatSelectChange } from "@angular/material/select";

@Component({
  selector: "boin-list-posts",
  templateUrl: "./list-posts.component.html",
  styleUrls: ["./list-posts.component.scss"]
})
export class ListPostsComponent implements OnInit {
  public posts: Post[];
  public users: User[];

  constructor(
    protected postService: PostsService,
    protected usersService: UsersService,
    protected snacks: MatSnackBar
  ) {}

  ngOnInit() {
    this.fetchPosts();
    this.fetchUsers();
  }

  public filterPostsByUser(change: MatSelectChange) {
    this.fetchPosts({ userId: change.value });
  }

  public fetchUsers() {
    this.usersService
      .listUsers()
      .subscribe(
        users => (this.users = users),
        () =>
          this.snacks.open(
            "Something went wrong, please try again later.",
            null,
            { duration: 2500 }
          )
      );
  }
  public fetchPosts(params?: { [key: string]: string }) {
    this.postService
      .listPosts(params)
      .subscribe(
        posts => (this.posts = posts),
        () =>
          this.snacks.open(
            "Something went wrong, please try again later.",
            null,
            { duration: 2500 }
          )
      );
  }

  public delete(id: number) {
    this.postService.removePost(id).subscribe(
      () =>
        this.snacks.open("Post successfully deleted.", null, {
          duration: 1500
        }),
      error =>
        this.snacks.open("Wasn't able to delete this post.", null, {
          duration: 1500
        })
    );
  }
}
