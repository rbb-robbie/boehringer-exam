import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatCardModule } from "@angular/material/card";
import { MatButtonModule } from "@angular/material/button";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";

import { PostsRoutingModule } from "./posts-routing.module";

import { ListPostsComponent } from "./list-posts/list-posts.component";
import { CreatePostComponent } from "./create-post/create-post.component";
import { PostDetailComponent } from "./post-detail/post-detail.component";
import { PostCommentsComponent } from "./post-comments/post-comments.component";

import { PostsService } from "./posts.service";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    ListPostsComponent,
    CreatePostComponent,
    PostDetailComponent,
    PostCommentsComponent
  ],
  imports: [
    CommonModule,
    PostsRoutingModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    MatSelectModule,
    MatInputModule
  ],
  providers: [PostsService]
})
export class PostsModule {}
