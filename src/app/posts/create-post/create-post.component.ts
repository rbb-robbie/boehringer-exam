import { Component, OnInit } from "@angular/core";
import { PostsService } from "../posts.service";
import { UsersService } from "src/app/users/users.service";
import { User } from "src/app/users/user";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "boin-create-post",
  templateUrl: "./create-post.component.html",
  styleUrls: ["./create-post.component.scss"]
})
export class CreatePostComponent implements OnInit {
  public postForm: FormGroup;
  public users: User[];

  constructor(
    protected router: Router,
    protected formBuilder: FormBuilder,
    protected postsService: PostsService,
    protected usersService: UsersService,
    protected snacks: MatSnackBar
  ) {}

  ngOnInit() {
    this.postForm = this.formBuilder.group({
      title: "",
      body: "",
      userId: null
    });
    this.usersService
      .listUsers()
      .subscribe(
        users => (this.users = users),
        () =>
          this.snacks.open(
            "Something went wrong, please try again later.",
            null,
            { duration: 2500 }
          )
      );
  }

  create() {
    this.postsService.newPost(this.postForm.value).subscribe(
      post =>
        this.snacks.open("New Post Successfully Created", null, {
          duration: 2500
        }),
      () =>
        this.snacks.open(
          "Something went wrong, please try again later.",
          null,
          { duration: 2500 }
        ),
      () => {
        this.router.navigate(["/post"]);
      }
    );
  }
}
