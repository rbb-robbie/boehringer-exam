import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Post, PostForm, PostComment } from "./post";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PostsService {
  constructor(protected http: HttpClient) {}

  public listPosts(
    queryParam: { [key: string]: any } = {}
  ): Observable<Post[]> {
    const handler = new Subject<Post[]>();
    this.http
      .get("https://jsonplaceholder.typicode.com/posts", {
        params: queryParam
      })
      .subscribe(
        (response: Post[]) => handler.next(response),
        (error: Error) => handler.error(error),
        () => handler.complete()
      );
    return handler.asObservable();
  }
  public postDetail(id: number): Observable<Post> {
    const handler = new Subject<Post>();
    this.http
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .subscribe(
        (post: Post) => handler.next(post),
        (error: Error) => handler.error(error),
        () => handler.complete()
      );
    return handler.asObservable();
  }
  public postComments(id: number): Observable<PostComment[]> {
    const handler = new Subject<PostComment[]>();
    this.http
      .get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
      .subscribe(
        (comments: PostComment[]) => handler.next(comments),
        (error: Error) => handler.error(error),
        () => handler.complete()
      );
    return handler.asObservable();
  }

  public newPost(values: PostForm): Observable<Post> {
    const handler = new Subject<Post>();
    this.http
      .post("https://jsonplaceholder.typicode.com/posts", values)
      .subscribe(
        (post: Post) => handler.next(post),
        (error: Error) => handler.error(error),
        () => handler.complete()
      );
    return handler.asObservable();
  }

  public removePost(id: number): Observable<null> {
    const handler = new Subject<null>();
    this.http
      .delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .subscribe(
        () => handler.next(),
        (error: Error) => handler.error(error),
        () => handler.complete()
      );
    return handler.asObservable();
  }
}
