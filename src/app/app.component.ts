import { Component } from '@angular/core';

@Component({
  selector: 'boin-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'posts-manager';
}
